---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:40% 90%](assets/openstack.png)

# Openstack

## Openstack CLI -
##  client en ligne de commande

-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Openstack CLI 

## Introduction
## Installation
## Configuration
## Commandes "Utilisateur"
## Commandes "Administrateur"

---

# Licence informations

Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)



---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Openstack CLI 

## Introduction

---
# Introduction

- Clients en ligne de commande
    - Pour chaque brique (nova-client, neutron, …)
    - Unifié "Openstack"
- Utiliser de préférence le Client unifié
    -  Commandes du type `openstack <ressource> <action>`
- S’appuie sur les API : peut-être utilisé à distance
- Gestion des projets, des utilisateurs, ...

---
# Utilisation

- Nécessite de configurer l’environnement de connexion à Openstack, au minimum :

  -  url keystone,
  -  user, paswword
  -  projet


- Exemple d'utilisation
```bash
openstack flavor list
openstack --help
openstack flavor list --help
```


---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Openstack CLI 

## Installation

---
# Depuis les dépots des distributions Linux

## Openstack Ubuntu pour Yoga

- Ajout du repository
```bash
sudo add-apt-repository -y cloud-archive:yoga
sudo apt update && sudo apt -y upgrade
```  
- Installation du client openstack
```bash
sudo apt install -y python-openstackclient
```
- Vérification
```bash
openstack  --help
``` 
---

# Avec python/pip

- Installation pour Zed
```bash
pip install python-openstackclient==6.0.*
# Pour Yoga = 5.8.*
# Pour Victoria = 5.4.*
```
- Vérification
```bash
openstack  --help
``` 

>**Attention aux versions**
>- *[https://docs.openstack.org/python-openstackclient/latest/cli/backwards-incompatible.html](https://docs.openstack.org/python-openstackclient/latest/cli/backwards-incompatible.html)*
>- *[https://docs.openstack.org/releasenotes/python-openstackclient/](https://docs.openstack.org/releasenotes/python-openstackclient/)*

---

<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Openstack CLI 

## Configuration

---
# Utilisation des parametres de la CLI

Lancer la commande « openstack » avec les informations en paramètre :

```bash
openstack \
--os-auth-url http://__OS_HOST__/identity/v3 \
--os-identity-api-version 3 \
--os-project-domain-name Default \
--os-user-domain-name Default \
--os-project-name __VOTRE_PROJET__ \
--os-username __VOTRE_LOGIN__ \
flavor list
```

---

# Utilisation des Variables d'environnement

Utilisation de variable d'environnement pour chaque paramètre :

- --os-auth-url  ==> OS_AUTH_URL
- --os-project-name ==> OS_PROJECT_NAME
- --os-username ==> OS_USERNAME
- ...
  
Exemple
```bash
export OS_USERNAME= __VOTRE_LOGIN__
echo $OS_USERNAME
```
---
# Utilisation d'un fichier RC

- Fichier contenant l'ensemble des variables a exporter
- Les variables sont "chargées" dans l'environnement avec la commande `source`
  
Exemple de fichier  mycloud.rc
```bash
export OS_USERNAME= __VOTRE_LOGIN__
export OS_PROJECT_NAME=__VOTRE_PROJET__
export OS_USER_DOMAIN_NAME=default
export OS_PASSWORD=__VOTRE_MOT_DE_PASSE__
export OS_AUTH_URL=http://__OS_HOST__/identity/v3
```

Utilisation
```bash
source mycloud.rc
env | grep OS_
```
---

# Utilisation d'Horizon pour le fichier RC

![bg right:50% 50%](img/rc-file.png)

Un fichier est disponible dans Horizon

- téléchargeable
- spécifique à chaque projet

---

# Configuration avec fichier yaml

Utilisé également par le SDK Openstack, et certaines applications tierces : 

```yaml
clouds:
  mycloud:
    region_name: RegionOne
    domain_name: Default
    auth:
      username: __VOTRE_LOGIN__
      password: __VOTRE_MOT_DE_PASSE__
      project_name: __VOTRE_PROJET__
      auth_url: https://__OS_HOST__:5000/v3
      identity_api_version: 3
```
Utilisation :  

```bash
openstack --os-cloud=mycloud flavor list
```
---

<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Openstack CLI 

## Commandes "Utilisateur"

---
# Découvrir son environnement



- Liste des clés disponibles
```bash
openstack keypair list
```
- Liste des gabarits
```bash
openstack flavor list
```
- Liste des images
```bash
openstack image list
```
- Liste des réseaux
```bash
openstack network list
```
---
# Gestion des Instances

- Création
``` bash
openstack server create \
  --key-name __KEY_NAME__ \
  --flavor __FLAVOR_ID__ \
  --image __IMAGE_ID__ \
  --nic net-id=__NETWORK_ID__ \
  --security-group __SECURITY_GROUP__ \
  __INSTANCE_NAME__
```
- Affichage des informations
```bash
openstack server list
openstack server show myserver
```
- Suppression
```bash
openstack server delete myserver
```
---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Openstack CLI 

## Commandes "Administrateur"

---
# Gestion des utilisateurs

- Création
```bash
openstack user create demo1
```
- Affichage
```bash
openstack user list
openstack user show demo1
```
- Modification
```bash
openstack user set --password demo1 demo1
openstack user set --email demo1@demo.fr demo1
openstack user set --project projet1 demo1
```
---
# Gestion des projets

- Création
```bash
openstack project create --description 'Projet de demo #1' projet1
```
- Affichage
```bash
openstack project list
openstack project show projet1
```

---

# Gestion des rôles

- Affichage de la liste des roles
```bash
openstack role list
openstack role list --user demo1 --project project1
```
- Affectation d’un rôle a un utilisateur
```bash
openstack role add --project project1 --user demo1 admin
```
---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Openstack CLI 

## Informations Complémentaires

---
# Informations

- [http://docs.openstack.org/user-guide/cli.html](http://docs.openstack.org/user-guide/cli.html)
- [http://docs.openstack.org/userguide/cli_cheat_sheet.html](http://docs.openstack.org/userguide/cli_cheat_sheet.html)
